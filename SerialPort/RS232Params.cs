﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace SerialPortLib
{
    public class RS232Params
    {
        #region 字段

        private string _PortName = string.Empty;   //端口名称
        private int _BaudRateValue = 0; //波特率
        private int _DataBitsValue = 0; //数据位长
        private StopBits _StopBitsValue = StopBits.None; //停止位数
        private Parity _ParityValue = Parity.None;   //校验
        private string _EndFlag = string.Empty;   //结束符

        #endregion

        #region 属性

        /// <summary>
        /// 串口名
        /// </summary>
        public string PortName
        {
            get { return _PortName; }
            set { _PortName = value; }
        }
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRateValue
        {
            get { return _BaudRateValue; }
            set { _BaudRateValue = value; }
        }
        /// <summary>
        /// 数据位长
        /// </summary>
        public int DataBitsValue
        {
            get { return _DataBitsValue; }
            set { _DataBitsValue = value; }
        }
        /// <summary>
        /// 停止位长
        /// </summary>
        public StopBits StopBitsValue
        {
            get { return _StopBitsValue; }
            set { _StopBitsValue = value; }
        }
        /// <summary>
        /// 校验位
        /// </summary>
        public Parity ParityValue
        {
            get { return _ParityValue; }
            set { _ParityValue = value; }
        }
        /// <summary>
        /// 数据结束符
        /// </summary>
        public string EndFlag
        {
            get { return _EndFlag; }
            set { _EndFlag = value; }
        }

        #endregion
    }
}
