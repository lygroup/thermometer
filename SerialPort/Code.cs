﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerialPortLib
{
    public class Code
    {
        private string _End = string.Empty;

        public Code() { }
        public Code(string end)
        {
            this._End = end;
        }
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Encode(string data)
        {
            return data + this._End;
        }
        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Decode(string data)
        {
            int _EndIndex = data.LastIndexOf(this._End);
            _EndIndex = _EndIndex < 0 ? data.Length : _EndIndex;
            return data.Substring(0, _EndIndex);
        }
        /// <summary>
        /// 是否结束
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool IsCompleted(string data)
        {
            bool _r = false;

            if (string.IsNullOrEmpty(this._End) || string.IsNullOrEmpty(data)) return true;
            
            int _EndIndex = data.LastIndexOf(this._End);
            if (_EndIndex >= 0) _r = true;

            return _r;
        }
    }
}
