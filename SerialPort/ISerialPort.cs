﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerialPortLib
{
    public delegate void DataReceivedHandler(object sender, DataReceivedEventArgs e);
    /// <summary>
    /// 串口接口
    /// </summary>
    public interface ISerialPort
    {
        int Open();
        int Send(string data);
        string Receive();
        int Close();
        event DataReceivedHandler ReceivedHandler;
    }

    /// <summary>
    /// 串口接收到数据触发事件参数基类
    /// </summary>
    public class DataReceivedEventArgs : EventArgs
    {
        public string ReceivedData { get; set; }

        public DataReceivedEventArgs() { }
        public DataReceivedEventArgs(string data)
            : this()
        {
            this.ReceivedData = data;
        }
    }
}
