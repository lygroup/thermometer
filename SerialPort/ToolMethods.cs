﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace SerialPortLib
{
    public class ToolMethods
    {
        public static string ConvertStopBitsTo(StopBits sb)
        {
            switch (sb)
            {
                case StopBits.None:
                    return "0";
                case StopBits.One:
                    return "1";
                case StopBits.OnePointFive:
                    return "1.5";
                case StopBits.Two:
                    return "2";
            }
            return string.Empty;
        }
        public static string ConvertParityTo(Parity p)
        {
            return Enum.GetName(typeof(Parity), p);
        }
        public static StopBits ConvertStopBitsStringTo(string stopbitString)
        {
            return (StopBits)Enum.Parse(typeof(StopBits), stopbitString,false);
        }
        public static Parity ConvertParityStringTo(string p)
        {
            return (Parity)Enum.Parse(typeof(Parity), p,false);
        }
    }
}
