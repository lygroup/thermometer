﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ThermometerBar
{
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }
        [Browsable(true), Category("Appearance"),DefaultValue("Engineer")]
        public String LabelText
        {
            get
            {
                return this.Lable1.Text;
            }
            set
            {
                this.Lable1.Text = value;
                this.Invalidate();
            }
        }
    }
}
