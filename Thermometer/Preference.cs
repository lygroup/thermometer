﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SerialPortLib;
using System.IO.Ports;

namespace Thermometer
{
    public delegate void UpdatePreferenceHandler(object sender,PreferenceEventArgs e);
    public partial class Preference : Form
    {
        public event UpdatePreferenceHandler UpdatePreference; 

        public Preference(RS232Params _params)
        {
            InitializeComponent();

            InitPortNames(_params.PortName);
            InitBaundRates(_params.BaudRateValue);
            InitDataBits(_params.DataBitsValue);
            InitStopBits(_params.StopBitsValue);
            InitParity(_params.ParityValue);
            InitEndFlag(_params.EndFlag);
        }

        #region 初始化窗口控件数据

        private void InitPortNames(string portname)
        {
            this.PortName_combo.Items.Clear();  //清空现存数据
            string[] _Names = RS232.AllEnablePortNames();   //获得串口名称数组
            //向ComboBox添加数据
            foreach (string item in _Names)
            {
                this.PortName_combo.Items.Add(item);
            }
            this.PortName_combo.SelectedItem = portname;   //设置值
        }
        private void InitBaundRates(int baundrate)
        {
            this.BaudRate_combo.Items.Clear();
            this.BaudRate_combo.Items.Add("110");
            this.BaudRate_combo.Items.Add("300");
            this.BaudRate_combo.Items.Add("600");
            this.BaudRate_combo.Items.Add("1200");
            this.BaudRate_combo.Items.Add("2400");
            this.BaudRate_combo.Items.Add("4800");
            this.BaudRate_combo.Items.Add("9600");
            this.BaudRate_combo.Items.Add("14400");
            this.BaudRate_combo.Items.Add("19200");
            this.BaudRate_combo.Items.Add("38400");
            this.BaudRate_combo.Items.Add("56000");
            this.BaudRate_combo.Items.Add("57600");
            this.BaudRate_combo.Items.Add("115200");
            this.BaudRate_combo.Items.Add("128000");
            this.BaudRate_combo.Items.Add("256000");

            this.BaudRate_combo.SelectedItem = baundrate.ToString();
        }
        private void InitDataBits(int databit)
        {
            this.DataBits_combo.Items.Clear();
            this.DataBits_combo.Items.Add("5");
            this.DataBits_combo.Items.Add("6");
            this.DataBits_combo.Items.Add("7");
            this.DataBits_combo.Items.Add("8");

            this.DataBits_combo.SelectedItem = databit.ToString();
        }
        private void InitStopBits(StopBits stopbit)
        {
            this.StopBits_combo.Items.Clear();
            this.StopBits_combo.Items.Add("1");
            this.StopBits_combo.Items.Add("1.5");
            this.StopBits_combo.Items.Add("2");

            this.StopBits_combo.SelectedItem = ToolMethods.ConvertStopBitsTo(stopbit).ToString();
        }
        private void InitParity(Parity p)
        {
            this.Parity_combo.Items.Clear();
            this.Parity_combo.Items.Add("None");
            this.Parity_combo.Items.Add("Odd");
            this.Parity_combo.Items.Add("Even");
            this.Parity_combo.Items.Add("Mark");
            this.Parity_combo.Items.Add("Space");

            this.Parity_combo.SelectedItem = ToolMethods.ConvertParityTo(p);
        }
        private void InitEndFlag(string end)
        {
            this.EndFlag_textbox.Text = end;
        }

        #endregion

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            PreferenceEventArgs pea = new PreferenceEventArgs();
            pea.PortName = this.PortName_combo.SelectedItem.ToString();
            pea.BaudRateValue = Convert.ToInt32(this.BaudRate_combo.SelectedItem);
            pea.DataBitsValue = Convert.ToInt32(this.DataBits_combo.SelectedItem);
            pea.StopBitsValue = ToolMethods.ConvertStopBitsStringTo(this.StopBits_combo.SelectedItem.ToString());
            pea.ParityValue = ToolMethods.ConvertParityStringTo(this.Parity_combo.SelectedItem.ToString());
            pea.EndFlag = this.EndFlag_textbox.Text;
            if (UpdatePreference != null)
            {
                UpdatePreference(this, pea);
            }
            this.Dispose();
        }

    }
}
