﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using SerialPortLib;
using System.IO.Ports;

namespace Thermometer
{
    public partial class Form1 : Form
    {
        RS232Params _RS232Param = new RS232Params();
        ISerialPort _SerialPort = null;
        public Form1()
        {
            InitializeComponent();
            //初始化
            _RS232Param.BaudRateValue = 9600;
            _RS232Param.DataBitsValue = 8;
            _RS232Param.StopBitsValue = StopBits.One;
            _RS232Param.ParityValue = Parity.None;
            _RS232Param.EndFlag = "0x0";
            _SerialPort = new RS232(_RS232Param);
        }
        

        [DllImport("dwmapi.dll")]
        public static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref MARGINS pMarginset);
        [StructLayout(LayoutKind.Sequential)]
        public struct MARGINS
        {
            public int Right;
            public int Left;
            public int Top;
            public int Bottom;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //if (System.Environment.OSVersion.Version.Major >= 6)
            //{
            //    this.BackgroundImage = null;
            //    MARGINS margins = new MARGINS();
            //    margins.Left = -1;
            //    margins.Right = -1;
            //    margins.Top = -1;
            //    margins.Bottom = -1;
            //    IntPtr hwnd = Handle;
            //    int result = DwmExtendFrameIntoClientArea(hwnd, ref margins);
            //    this.BackColor = Color.Black;
            //}
            //this.userControl11.LabelText = System.Environment.OSVersion.Version+"";
        }

        //设置
        private void button1_Click(object sender, EventArgs e)
        {
            Preference p = new Preference(_RS232Param);
            p.UpdatePreference += new UpdatePreferenceHandler(UpdatePreference);
            p.Show();
        }
        private void UpdatePreference(object sender, PreferenceEventArgs e)
        {
            this._RS232Param.BaudRateValue = e.BaudRateValue;
            this._RS232Param.DataBitsValue = e.DataBitsValue;
            this._RS232Param.EndFlag = e.EndFlag;
            this._RS232Param.ParityValue = e.ParityValue;
            this._RS232Param.PortName = e.PortName;
            this._RS232Param.StopBitsValue = e.StopBitsValue;

            this._SerialPort.Close();
            this._SerialPort = new RS232(this._RS232Param);
            this._SerialPort.ReceivedHandler += new DataReceivedHandler(ReceivedHandler);
        }
        private void ReceivedHandler(object sender, DataReceivedEventArgs e)
        {
            double _data=0d;
            if (string.IsNullOrEmpty(e.ReceivedData)) return;
            if (double.TryParse(e.ReceivedData, out _data))
            {
                this.bar1.UpdateProgressPercent((float)_data);
            }
        }

        //启动
        private void button2_Click(object sender, EventArgs e)
        {
            if (this._SerialPort != null)
            {
                this._SerialPort.Open();
            }
            else
            {
                MessageBox.Show("串口未实例化！");
            }
        }
        //关闭
        private void button3_Click(object sender, EventArgs e)
        {
            if (this._SerialPort != null)
            {
                this._SerialPort.Close();
            }
        }
    }
}
