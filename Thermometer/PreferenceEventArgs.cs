﻿using System;
using System.Collections.Generic;
using System.Text;
using SerialPortLib;
using System.IO.Ports;

namespace Thermometer
{
    public class PreferenceEventArgs:EventArgs
    {
        /// <summary>
        /// 串口号
        /// </summary>
        public string PortName { get; set; }
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRateValue { get; set; }
        /// <summary>
        /// 数据位
        /// </summary>
        public int DataBitsValue { get; set; }
        /// <summary>
        /// 停止位
        /// </summary>
        public StopBits StopBitsValue { get; set; }
        /// <summary>
        /// 校验位
        /// </summary>
        public Parity ParityValue { get; set; }
        /// <summary>
        /// 结束字符
        /// </summary>
        public string EndFlag { get; set; }
    }
}
