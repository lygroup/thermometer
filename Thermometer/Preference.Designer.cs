﻿namespace Thermometer
{
    partial class Preference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PortName_combo = new System.Windows.Forms.ComboBox();
            this.BaudRate_combo = new System.Windows.Forms.ComboBox();
            this.DataBits_combo = new System.Windows.Forms.ComboBox();
            this.StopBits_combo = new System.Windows.Forms.ComboBox();
            this.Parity_combo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.EndFlag_textbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(332, 257);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.EndFlag_textbox);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.Parity_combo);
            this.tabPage1.Controls.Add(this.StopBits_combo);
            this.tabPage1.Controls.Add(this.DataBits_combo);
            this.tabPage1.Controls.Add(this.BaudRate_combo);
            this.tabPage1.Controls.Add(this.PortName_combo);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(324, 231);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "串口参数";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(324, 231);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "串口号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "波特率";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "数据位";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "停止位";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "校验位";
            // 
            // PortName_combo
            // 
            this.PortName_combo.FormattingEnabled = true;
            this.PortName_combo.Location = new System.Drawing.Point(75, 21);
            this.PortName_combo.Name = "PortName_combo";
            this.PortName_combo.Size = new System.Drawing.Size(197, 20);
            this.PortName_combo.TabIndex = 5;
            // 
            // BaudRate_combo
            // 
            this.BaudRate_combo.FormattingEnabled = true;
            this.BaudRate_combo.Location = new System.Drawing.Point(75, 55);
            this.BaudRate_combo.Name = "BaudRate_combo";
            this.BaudRate_combo.Size = new System.Drawing.Size(197, 20);
            this.BaudRate_combo.TabIndex = 6;
            // 
            // DataBits_combo
            // 
            this.DataBits_combo.FormattingEnabled = true;
            this.DataBits_combo.Location = new System.Drawing.Point(75, 89);
            this.DataBits_combo.Name = "DataBits_combo";
            this.DataBits_combo.Size = new System.Drawing.Size(197, 20);
            this.DataBits_combo.TabIndex = 7;
            // 
            // StopBits_combo
            // 
            this.StopBits_combo.FormattingEnabled = true;
            this.StopBits_combo.Location = new System.Drawing.Point(75, 123);
            this.StopBits_combo.Name = "StopBits_combo";
            this.StopBits_combo.Size = new System.Drawing.Size(197, 20);
            this.StopBits_combo.TabIndex = 8;
            // 
            // Parity_combo
            // 
            this.Parity_combo.FormattingEnabled = true;
            this.Parity_combo.Location = new System.Drawing.Point(75, 157);
            this.Parity_combo.Name = "Parity_combo";
            this.Parity_combo.Size = new System.Drawing.Size(197, 20);
            this.Parity_combo.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "结束符";
            // 
            // EndFlag_textbox
            // 
            this.EndFlag_textbox.Location = new System.Drawing.Point(75, 191);
            this.EndFlag_textbox.Name = "EndFlag_textbox";
            this.EndFlag_textbox.Size = new System.Drawing.Size(197, 21);
            this.EndFlag_textbox.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(248, 275);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Preference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 305);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Preference";
            this.Text = "Preference";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox BaudRate_combo;
        private System.Windows.Forms.ComboBox PortName_combo;
        private System.Windows.Forms.ComboBox Parity_combo;
        private System.Windows.Forms.ComboBox StopBits_combo;
        private System.Windows.Forms.ComboBox DataBits_combo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox EndFlag_textbox;
        private System.Windows.Forms.Button button1;

    }
}